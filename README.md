# horizon-client-vm

To simplify setup of Horizon Client in a guest virtual machine

## Prerequisites

These programs should be installed before getting started

- make (used to abstract build, test, install, and uninstall commands)
- Packer (used to build the Vagrant box from an ISO and shell provisioners)
- Vagrant (used to interface with VirtualBox to configure and run the virtual machine)
- VirtualBox (used to run the guest virtual machine)

## Usage

Verify Packer template used to build the image

> make test

Use Packer to build the image

> make

Register the Horizon Client box with Vagrant

> make install

Configure and start the Horizon Client virtual machine

> make run
