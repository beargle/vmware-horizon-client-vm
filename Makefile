# Load environment variables from '.env' file if found
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

project_name = horizon-client
provisioners = provisioners/shell/lightdm.sh provisioners/shell/openbox.sh provisioners/shell/vbox-guest-additions.sh provisioners/shell/horizon-client.sh provisioners/shell/X.sh
sources = ks/ks.cfg $(provisioners) $(templates)
templates = $(project_name).pkr.hcl
targets = $(vagrant_box_file) output-virtualbox-iso/
vagrant_box_file = packer_$(project_name).box

.PHONY: clean default install packer-init packer-build packer-validate run test vagrant-box-add vagrant-box-remove vagrant-destroy vagrant-halt vagrant-up

default: clean packer-init packer-validate packer-build install run

clean:
	rm -rf $(targets)

install: vagrant-box-add

packer-build: packer-validate
	packer build -on-error=ask \
		$(templates)

packer-init:
	packer init \
		$(templates)

packer-validate: packer-init
	packer validate \
		$(templates)

run: vagrant-halt vagrant-destroy vagrant-up

test: packer-validate

vagrant-box-add:
	vagrant box add --force --name $(project_name) metadata.json

vagrant-box-remove:
	vagrant box remove --force $(project_name)

vagrant-destroy:
	vagrant destroy --force

vagrant-halt:
	vagrant halt

vagrant-up:
	vagrant up
