# 20250125

## Changed

- Default Makefile target to install and run guest
- VirtualBox guest additions provisioning script to simplify kernel package install
- Horizon Client version from 2209 to 2412, including removal of VMware-specific branding

# 20230507

## Added

- 3D acceleration, CPU, and memory to guest
- Configuration of default Horizon View URL from environment variable

## Changed

- Base operating system from CentOS 8.0.1905 to Rocky Linux 9.1
- Packer version from 1.3.5 to 1.8.4
- VMware Horizon Client version from 5.2.0 to 2209
- `make install` to preserve existing Vagrant box versions
- Format of packer template to match `packer fix` output
- Hardcoded ISO checksum to dynamic HTTP URL built from Rocky Linux version
- GNOME session settings to disable lock screen and screensaver
- Default view mode to fullscreen

## Removed

- Spaces workaround in Makefile to better support different `make` variants
- Support for host to guest clipboard in base image

# 20191215

## Added

- Packer template to create image from CentOS ISO and run provisioners for
    - CentOS package updates
    - GNOME desktop environment
    - VirtualBox Guest Additions
    - VMware Horizon Client
- Makefile to simplify running
    - Packer template test and image build
    - Vagrant box setup and run
