#!/bin/bash -eux

yum groups install -y base-x
yum install -y xterm
yum install -y libScrnSaver

# Start X session after login
systemctl set-default graphical.target
