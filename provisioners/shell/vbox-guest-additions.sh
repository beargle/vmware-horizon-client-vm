#!/bin/bash -eux
yum install -y \
    bzip2 \
    elfutils-libelf-devel \
    gcc \
    make \
    kernel-devel \
    kernel-headers \
    tar;

mkdir "${VBOX_GUEST_ADDITIONS_PATH}";
mount -o loop --read-only -t iso9660 "./${VBOX_GUEST_ADDITIONS_ISO}" "${VBOX_GUEST_ADDITIONS_PATH}";
"${VBOX_GUEST_ADDITIONS_PATH}/VBoxLinuxAdditions.run" --nox11;
