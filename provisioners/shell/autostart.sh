#!/bin/bash -eux

# Required for Horizon Client to autostart on login
mkdir -p /home/packer/.config/autostart
chown packer:packer /home/packer/.config
cat <<EOF > /home/packer/.config/autostart/horizon-client.desktop
[Desktop Entry]
Encoding=UTF-8
Type=Application
Icon=horizon-client.png
Exec=horizon-client --fullscreen --serverURL=${HORIZON_SERVER_URL}
MimeType=x-scheme-handler/horizon-client
Categories=Application;Network;
Name=Horizon Client
EOF
