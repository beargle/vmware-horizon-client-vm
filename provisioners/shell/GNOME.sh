#!/bin/bash -eux

dnf groups install -y GNOME
dnf install -y dbus-x11
dnf install -y libXScrnSaver
# Prevent welcome tour from loading on first login
dnf remove -y gnome-tour

# Start default target (runlevel) to graphical multi-user shell
systemctl set-default graphical.target

# Disable lock screen and screensaver
#
# 'dbus-launch' ensures packer-specific dbus session is running, required to
# persist GNOME settings changes.
sudo --user=packer dbus-launch gsettings set org.gnome.desktop.session idle-delay 0
sudo --user=packer dbus-launch gsettings set org.gnome.desktop.screensaver lock-enabled false
