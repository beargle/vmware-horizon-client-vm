#!/bin/bash -eux

# Ensure latest packages are installed and restart to pick up potential new
# kernel version
dnf update -y
reboot
