#!/bin/bash -eux

yum install -y openbox

# Initialize file for programs to autostart
mkdir -p /home/packer/.config/openbox
touch /home/packer/.config/openbox/autostart

# Start X session after login
systemctl set-default graphical.target
